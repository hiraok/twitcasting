package hiraok.dev.jp.twitcasting.ui.home

import android.arch.lifecycle.ViewModel
import hiraok.dev.jp.twitcasting.api.model.NewLive
import hiraok.dev.jp.twitcasting.repository.TwitCastingRepository
import io.reactivex.Flowable
import javax.inject.Inject

class HomeViewModel @Inject constructor(private val repository: TwitCastingRepository) : ViewModel() {
    fun getLive(): Flowable<List<NewLive>> {
        return repository.loadLive
    }
}
