package hiraok.dev.jp.twitcasting

import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import hiraok.dev.jp.twitcasting.di.DaggerAppComponent

class TwitCastingApplication : DaggerApplication() {
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder()
                .application(this)
                .build()
    }
}