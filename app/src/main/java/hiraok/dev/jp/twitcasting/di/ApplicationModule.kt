package hiraok.dev.jp.twitcasting.di

import dagger.Module
import dagger.Provides
import hiraok.dev.jp.twitcasting.BuildConfig
import hiraok.dev.jp.twitcasting.api.AuthHeader
import hiraok.dev.jp.twitcasting.api.TwitCastingService
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
class ApplicationModule {

    @Singleton
    @Provides
    fun provideOkHttpClient(): OkHttpClient = OkHttpClient.Builder().addInterceptor {
        val header = AuthHeader.createHeader(BuildConfig.ACCESS_TOKEN)
        val request = it.request().newBuilder().headers(header).build()
        it.proceed(request)
    }.build()

    @Singleton
    @Provides
    fun provideRetrofit(): Retrofit =
            Retrofit.Builder().baseUrl(BuildConfig.BASE_URL).addConverterFactory(
                    GsonConverterFactory.create()).build()

    @Singleton
    @Provides
    fun provideTwitCastingService(retrofit: Retrofit): TwitCastingService =
            retrofit.create(TwitCastingService::class.java)
}