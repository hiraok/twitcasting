package hiraok.dev.jp.twitcasting.di

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import hiraok.dev.jp.twitcasting.ui.home.HomeViewModel
import hiraok.dev.jp.twitcasting.viewmodel.TwitCastingViewModelFactory

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    abstract fun bindHomeViewModel(homeViewModel: HomeViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: TwitCastingViewModelFactory): ViewModelProvider.Factory

}